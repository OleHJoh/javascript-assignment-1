
//Setts up global values
const URL = "https://noroff-komputer-store-api.herokuapp.com/computers"
const laptopPicture = document.getElementById("laptopPicture")
const laptopSelected = document.getElementById("laptopSelected")
let laptops = []
const getLoanBtn = document.getElementById("getLoan")
const depositMoneyBtn = document.getElementById("bank")
const workBtn = document.getElementById("work")
const buyLaptopBtn = document.getElementById("buyLaptop")
const payLoanBtn = document.getElementById("payLoan")
let money = 0;
let moneySaved = 0;
let bankLoan = 0;
let computerValue = 0
let computersOwned = []

//Renders the different elements in the view and sett the webpage up for start
function renderLaptops(laptopSelected, laptops){
    for(let i = 0; i < laptops.length; i++){
        const html = `
            <option value="${i}">
            ${laptops[i].title}
            </option>
        `   
        laptopSelected.insertAdjacentHTML("beforeend", html)
    };

    printPicture(0)
    printName(0)
    printDescription(0)
    printValue(0)
    printSpecs(0)
    printBankMoney()
    printEarnedMoney()
    payLoanBtn.style.display = "none"
    document.getElementById("loanList").style.visibility = "hidden"
}

//The script for the work button
//Also check the loan to see if the money shall be deducted from the work
workBtn.addEventListener("click", function(){
    if(checkLoan() == true){
        let payPercentage = 0.1
        bankLoan = bankLoan - (100 * payPercentage)
        money = money + (100 - (100 * payPercentage))
        printEarnedMoney()
        printLoan()
        checkLoan()
    }
    else{
        money = money + 100
        printEarnedMoney()
    }
})

//The script for the deposit button
//Moves the money earned from to the bank
depositMoneyBtn.addEventListener("click", function(){
    moneySaved = moneySaved + money
    money = 0
    printBankMoney()
    printEarnedMoney()
})

//The script fro the get a loan button
//Checks if the user have a loan so the user don't have two at the same time
//Checks that only numbers are typed inn, as well as leaving it blank
//When all criterias are met gives the user the loan
getLoanBtn.addEventListener("click", function(){
    if(checkLoan() == true){
        throw(alert("You already have a loan on: " + bankLoan))
    }
    let popUp = window.prompt("How much are you hoping to loan?")
    if(isNaN(popUp) == true){
        throw(alert("That wasn't a number"))
    }
    else if(popUp > moneySaved *2){
        throw(alert("You can't loan that much"))
    }
    else if(popUp == ""){
        throw(alert("You left it blank"))
    }
    else
        bankLoan = parseInt(popUp)
        moneySaved = parseInt(popUp) + moneySaved
        printBankMoney()
        printLoan()
        checkLoan()
})

//The script for the pay loan button
//It checks if the user have enough money to pay it back, and tells the user if they don't
//When the user have enough money it let's them pay back the loan
payLoanBtn.addEventListener("click", function(){
    if(money < bankLoan){
        throw(alert("You don't have enough to pay back the loan"))
    }
    money = money - bankLoan
    bankLoan = 0
    checkLoan()
    printEarnedMoney()
})

//The script for what laptop is shown by selecting a laptop from the list
laptopSelected.addEventListener("change", function(){

    let computer = laptopSelected.selectedIndex

    printPicture(computer)
    printName(computer)
    printDescription(computer)
    printValue(computer)
    printSpecs(computer)
})

//The script for the buy laptop button
//It checks if the user have enough money to buy the selected computer
//And tells the user if they don't have enough
//If the user already own the selected computer it will tell them
//It will tell the user when they buy a new computer and when the user have bought them all
buyLaptopBtn.addEventListener("click", function(){
    if(moneySaved < computerValue){
        throw(alert("You don't have enough founding for this computer"))
    }
    if(computersOwned.includes(laptopSelected.selectedIndex)){
        if(confirm("Are you sure you want to buy this computer again")){
            moneySaved = moneySaved - computerValue
            throw(alert("You bought this computer again"))
        }
        else{
            throw(alert("You didn't buy it again"))
        }
    }
    moneySaved = moneySaved - computerValue
    computersOwned.push(laptopSelected.selectedIndex)
    printBankMoney()
    if(computersOwned.length === laptops.length){
        throw(alert("You now own every computer on this store!!!"))
    }
    else{
        throw(alert("You now own a new computer"))
    }
})





//The fetch function to get the data from the api
//Also initiate the render function
fetch(URL)
    .then(function (response){
        return response.json()
    })
    .then(function(_laptops){
        laptops = _laptops
        console.log(laptops)
        renderLaptops(laptopSelected, laptops)
    })
    .catch(function(error){
        console.error(error.message)
    })

//Print function for the computer pictures
function printPicture(id){
    laptopPicture.src = `https://noroff-komputer-store-api.herokuapp.com/${laptops[id].image}`
}

//Print function for the computer names
function printName(id){
    const laptopTitle= `${laptops[id].title}`
    laptopName.innerHTML = laptopTitle
}

//Print function for the computer descriptions
function printDescription(id){
    const laptopDesc = `${laptops[id].description}`
    laptopDescription.innerHTML = laptopDesc
}

//Print function for the computer prices
function printValue(id){
    const laptopValue = `${laptops[id].price}`
    laptopPrice.innerHTML = laptopValue
    computerValue = laptops[id].price
}

//Print function for the computer specs
function printSpecs(id){
    var laptopFeats = laptops[id].specs
    laptopFeatures.innerHTML = ""
    for (let i = 0; i < laptopFeats.length; i++) {
        const laptopFeatsPrint = `
            <li>
                <p style="font-size: 14px;">${laptopFeats[i]}</p>
            </li>
        `
        laptopFeatures.insertAdjacentHTML("beforeend", laptopFeatsPrint)
    }
}

//Print function for the money in the bank
function printBankMoney(){
    document.getElementById("bankMoney").innerHTML = moneySaved
}

//Print function for the money earned from work
function printEarnedMoney(){
    document.getElementById("earnedMoney").innerHTML = money
}

//Function for checking if the user have a loan
//Shows the loan in the bank and switches the deposit button with pay back loan button
function checkLoan(){
    if(bankLoan > 0){
        payLoanBtn.style.display = "initial"
        document.getElementById("loanList").style.visibility = "visible"
        depositMoneyBtn.style.display = "none"
        return true
    }
    else{
        payLoanBtn.style.display = "none"
        document.getElementById("loanList").style.visibility = "hidden"
        depositMoneyBtn.style.display = "initial"
        return false
    }
}

//Print function for the users loan
function printLoan(){
    document.getElementById("loan").innerHTML = bankLoan
}

//Error function if a picture ain't found
laptopPicture.addEventListener("error", () => {
    laptopPicture.src = "../Bilder/imageNotFound.jpg"
  })